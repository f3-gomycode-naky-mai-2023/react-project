import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Formulaire from './components/Formulaire';
import Images from './components/Image';

function App() {
  return (
    <div className="App">
      <Header></Header>
      <Formulaire/>
      <Images></Images>
    </div>
  );
}

export default App;
